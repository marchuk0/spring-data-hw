package com.bsa.springdata.user;

import org.hibernate.boot.model.source.spi.Orderable;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    @Query("SELECT u FROM User u " +
            "LEFT JOIN FETCH u.team t " +
            "LEFT JOIN FETCH u.office " +
            "LEFT JOIN FETCH t.technology " +
            "LEFT JOIN FETCH t.project " +
            "WHERE lower(u.lastName) LIKE lower(concat(:lastName, '%'))")
    List<User> findByLastNameStartsWithIgnoreCase(String lastName, Pageable pageable);

    @Query("SELECT u FROM User u " +
            "LEFT JOIN FETCH u.team t " +
            "LEFT JOIN FETCH u.office " +
            "LEFT JOIN FETCH t.technology " +
            "LEFT JOIN FETCH t.project " +
            "WHERE u.office.city = :city " +
            "ORDER BY u.lastName ASC")
    List<User> findByCity(String city);

    @Query("SELECT u FROM User u " +
            "LEFT JOIN FETCH u.team t " +
            "LEFT JOIN FETCH u.office " +
            "LEFT JOIN FETCH t.technology " +
            "LEFT JOIN FETCH t.project " +
            "WHERE u.experience >= :experience " +
            "ORDER BY u.experience DESC")
    List<User> findByExperience(int experience);

    @Query("SELECT u FROM User u " +
            "LEFT JOIN FETCH u.team t " +
            "LEFT JOIN FETCH u.office " +
            "LEFT JOIN FETCH t.technology " +
            "LEFT JOIN FETCH t.project " +
            "WHERE u.team.room = :room AND u.office.city = :city"
            )
    List<User> findByRoomAndCity(String room, String city, Sort sort);

    int deleteByExperienceIsLessThan(int experience);
}
