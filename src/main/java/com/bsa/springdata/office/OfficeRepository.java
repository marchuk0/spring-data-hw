package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.swing.text.html.Option;
import javax.transaction.Transactional;
import javax.validation.groups.ConvertGroup;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {

    @Query(value = "SELECT DISTINCT o.id, o.city, o.address " +
            "FROM offices o " +
            "JOIN users u ON u.office_id = o.id " +
            "JOIN teams t ON u.team_id = t.id " +
            "JOIN technologies tech ON tech.id = t.technology_id " +
            "WHERE tech.name = ?1",
    nativeQuery = true)
    List<Office> findAllByTechnology(String technology);

    @Modifying
    @Transactional
    @Query(value = "UPDATE offices o " +
            "SET address = ?2 " +
            "FROM users u " +
            "WHERE u.office_id = o.id AND o.address = ?1",
    nativeQuery = true)
    int updateAddress(String oldAddress, String newAddress);

    Optional<Office> findByAddress(String address);


}
