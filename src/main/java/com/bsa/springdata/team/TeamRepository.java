package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {

    @Modifying
    @Transactional
    @Query(value = "UPDATE teams t\n" +
            "SET name = concat(t.name, '_', projects.name)\n" +
            "FROM projects\n" +
            "WHERE projects.id = t.project_id;\n" +
            "\n" +
            "UPDATE teams t\n" +
            "SET name = concat(t.name, '_', technologies.name)\n" +
            "FROM technologies\n" +
            "WHERE technologies.id = t.technology_id;\n",
    nativeQuery = true)
    void normalizeName(String team);

    Optional<Team> findByName(String name);

    @Query(value = "SELECT COUNT(t.id) " +
            "FROM teams t " +
            "JOIN technologies tech ON t.technology_id = tech.id " +
            "WHERE tech.name = ?1",
    nativeQuery = true)
    int countByTechnologyName(String newTechnology);
}
