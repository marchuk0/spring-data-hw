package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.lang.management.OperatingSystemMXBean;
import java.util.Optional;
import java.util.UUID;

public interface TechnologyRepository extends JpaRepository<Technology, UUID> {
    @Modifying
    @Transactional
    @Query(value = "UPDATE technologies tech " +
            "SET name = ?3 " +
            "FROM (SELECT t.technology_id AS techid " +
            "       FROM teams t " +
            "       LEFT JOIN users u ON u.team_id = t.id " +
            "       GROUP BY t.id " +
            "       HAVING count(u.id) < ?1) AS subquery " +
            "WHERE tech.name = ?2 AND tech.id = subquery.techid",
    nativeQuery = true)
    void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName);

    Optional<Technology> findByName(String name);
}
