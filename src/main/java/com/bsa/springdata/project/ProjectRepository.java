package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {
    @Query(value = "SELECT p.id, p.name, p.description " +
            "FROM projects p " +
            "LEFT JOIN teams t ON t.project_id = p.id " +
            "LEFT JOIN technologies tech ON tech.id = t.technology_id " +
            "LEFT JOIN users u ON u.team_id = t.id " +
            "WHERE tech.name = ?1 " +
            "GROUP BY p.id " +
            "ORDER BY COUNT(u.id) ASC " +
            "LIMIT 5",
    nativeQuery = true)
    List<Project> findTop5ByTechnology(String technology);

    @Query(value = "SELECT p.id, p.name, p.description " +
            "FROM projects p " +
            "LEFT JOIN teams ON teams.project_id = p.id " +
            "LEFT JOIN users ON users.team_id = teams.id " +
            "GROUP BY p.id " +
            "ORDER BY count(teams.id) DESC, count(users.id) DESC, p.name DESC " +
            "LIMIT 1 ",
    nativeQuery = true)
    Optional<Project> findTheBiggestProject();

    @Query(value = "SELECT p.name as name, " +
            "COUNT(DISTINCT t.id) as \"teamsNumber\", " +
            "COUNT(DISTINCT u.id) as \"developersNumber\", " +
            "STRING_AGG(DISTINCT tech.name, ',' ORDER BY tech.name DESC) as technologies " +
            "FROM projects p " +
            "LEFT JOIN teams t ON t.project_id = p.id " +
            "LEFT JOIN technologies tech ON t.technology_id = tech.id " +
            "LEFT JOIN users u ON u.team_id = t.id " +
            "GROUP BY p.id " +
            "ORDER BY p.name ASC",
    nativeQuery = true)
    List<ProjectSummaryDto> getSummary();

    @Query(value = "SELECT count(DISTINCT p.id) " +
            "FROM projects p " +
            "JOIN teams t ON t.project_id = p.id " +
            "JOIN users u ON u.team_id = t.id " +
            "JOIN user2role ur ON ur.user_id = u.id " +
            "JOIN roles r ON ur.role_id = r.id " +
            "WHERE r.name = ?1",
    nativeQuery = true)
    int getCountWithRole(String role);


}