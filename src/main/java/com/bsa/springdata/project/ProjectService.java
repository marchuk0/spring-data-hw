package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.team.Technology;
import com.bsa.springdata.team.TechnologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TechnologyRepository technologyRepository;
    @Autowired
    private TeamRepository teamRepository;

    public List<ProjectDto> findTop5ByTechnology(String technology) {
        // TODO: Use single query to load data. Sort by number of developers in a project
        //  Hint: in order to limit the query you can either use native query with limit or Pageable interface
        return projectRepository
                .findTop5ByTechnology(technology)
                .stream()
                .map(ProjectDto::fromEntity)
                .collect(Collectors.toList());
    }

    public Optional<ProjectDto> findTheBiggest() {
        // TODO: Use single query to load data. Sort by teams, developers, project name
        //  Hint: in order to limit the query you can either use native query with limit or Pageable interface
       return projectRepository
               .findTheBiggestProject()
               .map(ProjectDto::fromEntity);
    }

    public List<ProjectSummaryDto> getSummary() {
        // TODO: Try to use native query and projection first. If it fails try to make as few queries as possible
        return projectRepository.getSummary();
    }

    public int getCountWithRole(String role) {
        // TODO: Use a single query
        return projectRepository.getCountWithRole(role);
    }

    public UUID createWithTeamAndTechnology(CreateProjectRequestDto createProjectRequest) {
        // TODO: Use common JPARepository methods. Build entities in memory and then persist them
        var technology = Technology
                .builder()
                .description(createProjectRequest.getProjectDescription())
                .link(createProjectRequest.getTechLink())
                .name(createProjectRequest.getTech())
                .build();
        var project = Project
                .builder()
                .description(createProjectRequest.getProjectDescription())
                .name(createProjectRequest.getProjectName())
                .build();
        var createdTechnology = technologyRepository.save(technology);
        var createdProject = projectRepository.save(project);
        var team = Team
                .builder()
                .room(createProjectRequest.getTeamRoom())
                .name(createProjectRequest.getTeamName())
                .area(createProjectRequest.getTeamArea())
                .project(createdProject)
                .technology(createdTechnology)
                .build();
        teamRepository.save(team);
        return createdProject.getId();
    }
}
