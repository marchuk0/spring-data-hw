package com.bsa.springdata.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.transaction.Transactional;
import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {

    @Modifying
    @Transactional
    @Query(value = "DELETE\n" +
            "FROM roles r\n" +
            "WHERE (r.code = ?1) AND\n" +
            "r.id IN (SELECT r.id\n" +
            "\t\tFROM roles r\n" +
            "\t\tLEFT JOIN user2role ur ON r.id = ur.role_id \n" +
            "\t\tWHERE ur.role_id IS NULL)",
    nativeQuery = true)
    void deleteByRole(String roleCode);
}
